$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
      interval: 2000
    });

    $('#contacto').on('show.bs.modal', function (e) {
      console.log('El modal se esta mostrando');

        $('.contactBoton').removeClass('btn-outline-succes');
        $('.contactBoton').addClass('btn-primary');
        $('.contactBoton').prop('disable', true);
      });

    $('#contacto').on('shown.bs.modal', function (e) {
      console.log('El modal se esta mostró');
      });

    $('#contacto').on('hide.bs.modal', function (e) {
      console.log('El modal se oculta');
      });

    $('#contacto').on('hidden.bs.modal', function (e) {
      console.log('El modal se oculto');
      $('.contactBoton').removeClass('btn-primary');
      $('.contactBoton').addClass('btn-outline-succes');
      $('.contactBoton').prop('disable', false);
      });
  });